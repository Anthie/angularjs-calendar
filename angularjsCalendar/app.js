﻿var calendarApp = angular.module('calendarApp', [])

var _currentDate = new Date();
var _currentMonth = _currentDate.getMonth();
var eventList = [];

calendarApp.controller('eventListController', function ($scope) {

    $scope.getAllEvents = function (parentIndex) {
        return eventList;
    };
});

calendarApp.controller('mainController', function ($scope) {

    $scope.modalAppear = false;

    var currentDayIndex = null;
    var nameToDelete = null;
    var timeToDeltet = null;

    $scope.showModal = function (show, eventType) {
        $scope.modalAppear = show;
        $scope.eventEdit = false;
        $scope.eventHeader = "New Event";
        $scope.backdrop = "backdropLight";


        if (show) {
            $scope.backdrop = "backdropDim";
            currentDayIndex = this.$index;
            $scope.eventName = "";
            $scope.eventTime = "";
        }

        if (eventType) {
            $scope.eventEdit = true;
            $scope.eventHeader = "Edit Event";
            nameToDelete = this.e.name;
            timeToDelete = this.e.time;
            $scope.eventName = this.e.name;
            $scope.eventTime = this.e.time;
        }
    };

    $scope.setCurrentMonth = function (index) {
        _currentDate.setMonth(_currentDate.getMonth() + index, 1);
        _currentMonth = _currentDate.getMonth();
    };

    $scope.getEvents = function (parentIndex) {
        if (eventList.length === 0) { return; }

        var dayEventList = []
        for (var i = 0; i < eventList.length; i++) {
            if (parentIndex === eventList[i].index && _currentMonth == eventList[i].month) {
                dayEventList.push(eventList[i])
            }
        }
        return dayEventList;
    };

    $scope.deleteEvent = function () {
        for (var i = 0; i < eventList.length; i++) {
            if (nameToDelete === eventList[i].name && timeToDelete === eventList[i].time) {
                eventList.splice(i, 1);
                break;
            }
        }
    };

    $scope.saveEvent = function (name, time) {
        $scope.eventName = name;
        $scope.eventTime = getTime(time);
        var event = { index: currentDayIndex, name: name, time: $scope.eventTime, month: _currentMonth };
        eventList.push(event);
    };
});

calendarApp.controller('headerController', function ($scope) {
    $scope.date = _currentDate;
});

calendarApp.controller('bodyController', function ($scope) {

    var weeks = [];
    $scope.getWeeks = function () {
        var currentYear = _currentDate.getFullYear();
        var currentMonth = _currentDate.getMonth();
        var numDays = daysInMonth(currentMonth, currentYear);
        var currentWeek = null;
        weeks = [];

        for (var i = 1; i <= numDays; i++) {
            if (!currentWeek) {
                currentWeek = new Week();
            }

            var currentDate = new Date(currentYear, currentMonth, i);
            if (currentWeek.isFull()) {
                weeks.push(currentWeek);
                currentWeek = new Week();
            }
            currentWeek.addDate(currentDate);
        }
        if (!currentWeek.isEmpty()) {
            if (!currentWeek.isFull()) {
                var currentDate = new Date(currentYear, currentMonth, numDays);
                currentWeek.fillAdditionalDates(currentDate);
            }
            weeks.push(currentWeek);
        }
        return new Array(1);
    }

    $scope.getDays = function () {
        var days = []
        for (var i = 0; i < weeks.length; i++) {
            for (var j = 0; j < 7; j++) {
                days.push(weeks[i].getDates()[j].getDate());
            }
        }
        return days;
    }

    $scope.isOtherMonthDays = function (index) {
        var weekIndex = (parseInt(index / 7, 10));
        var dayIndex = (index % 7);
        var givenMonth = weeks[weekIndex].getDates()[dayIndex].getMonth()
        if (givenMonth != _currentMonth)
            return true;
        return false;
    };
});


calendarApp.controller('modalController', function ($scope) {

});


//
// Helper Functions
//
function daysInMonth(month, fullYear) {
    return new Date(fullYear, month + 1, 0).getDate();
}

function getTime(time) {
    var ampm = (time.getHours() >= 12) ? "PM" : "AM";
    return hours12Format(time.getHours()) + ":" + minutesFormat(time.getMinutes()) + " " + ampm;
}

function hours12Format(hours) {
    return (hours + 24) % 12 || 12;
}

function minutesFormat(min) {
    if (min === 0)
        return "00";
    else if (min < 10)
        return "0" + min;
    return min;
}

//
// Week class
//
function Week() {
    var self = this;
    var dates = [];

    self.addDate = function (date) {
        addMissingDatesIfNeeded(date);
        dates.push(date);
    };

    self.isFull = function () {
        return dates.length === 7;
    };

    self.isEmpty = function () {
        return dates.length === 0;
    };

    self.getDates = function () {
        return dates;
    }

    self.fillAdditionalDates = function (date) {
        var numDatesNeeded = 6 - date.getDay();

        for (var i = 1; i <= numDatesNeeded; i++) {
            var dateToAdd = new Date(date);
            dateToAdd.setDate(date.getDate() + i);
            dates.push(dateToAdd);
        }
    }

    function addMissingDatesIfNeeded(date) {
        var needPrevousDates = date.getDay() != dates.length;

        if (needPrevousDates) {
            fillPreviousDates(date);
        }
    }

    function fillPreviousDates(date) {
        var numDatesNeeded = date.getDay() - dates.length;

        for (var i = 0; i < numDatesNeeded; i++) {
            var dateToAdd = new Date(date);
            var numDaysInPast = numDatesNeeded - i;
            dateToAdd.setDate(date.getDate() - numDaysInPast);
            dates.push(dateToAdd);
        }
    }
}